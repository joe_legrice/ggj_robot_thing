﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Damagable : MonoBehaviour
{
    public int m_health = 100;
    public GameObject m_destructionEffect;
    public float m_invincibilityCooldown;
    public float m_pulsationTime;
    float m_invincibilityCooldownTimer;

    SpriteRenderer[] m_spriteRenderers;
    Color[] m_spriteColours;

    public float GetHealth()
    {
        return m_health;
    }

    void Start()
    {
        m_spriteRenderers = GetComponentsInChildren<SpriteRenderer> ();
        m_spriteColours = new Color[m_spriteRenderers.Length];
        for (int i = 0; i < m_spriteRenderers.Length; i++)
        {
            m_spriteColours [i] = m_spriteRenderers [i].color;
        }
    }

    void Update()
    {
        if (m_invincibilityCooldownTimer > 0.0f)
        {
            m_invincibilityCooldownTimer -= Time.deltaTime;

            bool red = false;
            if (m_invincibilityCooldownTimer > 0.0f)
            {
                if ((m_invincibilityCooldownTimer % m_pulsationTime) > m_pulsationTime * 0.5f)
                {
                    red = true;
                }
            }
            for(int i = 0; i < m_spriteRenderers.Length; i++)
            {
                Color currentColour = red ? Color.Lerp(m_spriteColours [i], Color.red, 0.5f) : m_spriteColours [i];
                m_spriteRenderers[i].color = currentColour;
            }
        }
    }

    public void Damage(int damage)
    {
        if (m_invincibilityCooldownTimer <= 0.0f)
        {
            m_health -= damage;
            m_invincibilityCooldownTimer = m_invincibilityCooldown;
            if (m_health <= 0 && m_destructionEffect != null)
            {
                GameObject.Instantiate (m_destructionEffect, transform.position, Quaternion.identity);
                Destroy (gameObject);
            }
        }
    }
}
