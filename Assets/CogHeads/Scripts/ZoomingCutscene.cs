﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoomingCutscene : MonoBehaviour {

    Vector3 m_startPosition;
    bool m_started;
    public float m_triggerDistance = 10.0f;
    public float m_followSpeed;
    public Transform m_followTransform;
    public float m_followTargetZoom;
    public int m_inRangeCounter = 0;
    public int m_inRangeCounterTarget = 5;
    bool m_wasInRange = false;
	// Use this for initialization
	void Start () {
        m_startPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        bool inRange = Vector3.SqrMagnitude (transform.position - m_startPosition) < m_triggerDistance * m_triggerDistance;
        if (!m_started && transform.position != m_startPosition && !inRange)
        {
            FollowCamera followCamera = FindObjectOfType<FollowCamera> ();
            followCamera.m_target = m_followTransform;
            followCamera.m_followSpeed = m_followSpeed;
            followCamera.m_targetZoom = m_followTargetZoom;
            GetComponent<AudioSource> ().Play ();

            m_started = true;
        }
        else if (m_started && inRange && !m_wasInRange)
        {
            m_inRangeCounter++;

            if (m_inRangeCounter >= m_inRangeCounterTarget)
            {
                LevelManager.Instance.TryGoToNextLevel ();
            }
        }
        m_wasInRange = inRange;
	}
}
