﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
	Rigidbody2D m_rigidBody;

	public float m_movementForce = 1.0f;
	public GameObject m_eyebrows;
    public float m_minLegY = 0.0f;
    public float m_maxLegY = 1.0f;
    public float m_legYChangeSpeed = 1.0f;
    public float m_jumpForce = 100.0f;
    public float m_jumpCooldown = 1.0f;
    public float m_jumpRaycastDownDistance = 2.0f;
    public float m_jumpEdgeX = 0.5f;

    float m_jumpCooldownTimer;
    int m_jumpRaycastLayerMask;
	HingeJoint2D m_hingeJoint;
	bool m_wasLocked = false;
	Connectable m_myConnectable;
	Connectable[] m_allConnectables;
    DistanceJoint2D m_distanceJoint;
    Vector2 m_relativeAnchor;
    Inventory m_inventory;
    AudioSource m_audioSource;

	List<Connectable> m_closestConnectables;

    public List<Connectable> GetClosestConnectables()
    {
        return m_closestConnectables;
    }

    public bool IsLocked()
    {
        return m_wasLocked;
    }

	void Start()
	{
		m_rigidBody = GetComponent<Rigidbody2D>();
		m_hingeJoint = GetComponent<HingeJoint2D> ();
		m_myConnectable = GetComponent<Connectable> ();
		m_allConnectables = FindObjectsOfType<Connectable> ();
		m_closestConnectables = new List<Connectable> ();
        m_distanceJoint = GetComponent<DistanceJoint2D> ();
        m_jumpRaycastLayerMask = LayerMask.GetMask ("Default", "Mechanisms");
        m_inventory = GetComponentInParent<Inventory> ();
        m_audioSource = GetComponent<AudioSource> ();
	}
	List<Connectable> FindClosestConnectables()
	{
		float closestRange = m_myConnectable.m_lockingRadius * 2.0f;
		//Connectable closestConnectable = null;
		m_closestConnectables.Clear();
		for (int i = 0; i < m_allConnectables.Length; i++)
		{
			Connectable c = m_allConnectables [i];
			if (c != null && c != m_myConnectable)
			{
                float distance = Vector3.Distance (c.transform.position, transform.position) - c.m_lockingRadius;
				if (distance < closestRange)
				{
					//closestRange = distance;
					m_closestConnectables.Add(c);
				}
			}
		}
		return m_closestConnectables;
	}

    Vector2 GetLockingPosition(List<Connectable> closestConnectables)
    {
        float lockingDistanceA = m_myConnectable.m_lockingRadius + closestConnectables [0].m_lockingRadius;
        PhysicsUtils.Circle circleA = new PhysicsUtils.Circle ();
        circleA.p = closestConnectables [0].GetAnchorPosition ();
        circleA.radius = lockingDistanceA;

        float lockingDistanceB = m_myConnectable.m_lockingRadius + closestConnectables [1].m_lockingRadius;
        PhysicsUtils.Circle circleB = new PhysicsUtils.Circle ();
        circleB.p = closestConnectables [1].GetAnchorPosition ();
        circleB.radius = lockingDistanceB;

        List<Vector2> points = PhysicsUtils.Intersect (circleA, circleB);
        float closestDistSqrd = float.MaxValue;
        Vector2 closestPoint = Vector2.zero;
        foreach(Vector2 point in points)
        {
            float distSqrd = (point - new Vector2(transform.position.x, transform.position.y)).sqrMagnitude;
            if (distSqrd < closestDistSqrd)
            {
                closestPoint = point;
                closestDistSqrd = distSqrd;
            }
        }
        return closestPoint;
    }

    void HandleJumping()
    {
        if (Input.GetButton("Jump"))
        {
            if (m_jumpCooldownTimer <= 0.0f)
            {
                RaycastHit2D rayHit = Physics2D.Raycast (transform.position, new Vector2 (0.0f, -1.0f), m_jumpRaycastDownDistance + m_distanceJoint.connectedAnchor.y, m_jumpRaycastLayerMask);
                if (rayHit.collider == null)
                {
                    rayHit = Physics2D.Raycast (transform.position + new Vector3(m_jumpEdgeX, 0.0f), new Vector2 (0.0f, -1.0f), m_jumpRaycastDownDistance + m_distanceJoint.connectedAnchor.y, m_jumpRaycastLayerMask);
                }
                if (rayHit.collider == null)
                {
                    rayHit = Physics2D.Raycast (transform.position + new Vector3(-m_jumpEdgeX, 0.0f), new Vector2 (0.0f, -1.0f), m_jumpRaycastDownDistance + m_distanceJoint.connectedAnchor.y, m_jumpRaycastLayerMask);
                }
                if (rayHit.collider != null)
                {
                    Debug.Log ("Jumping");
                    m_jumpCooldownTimer = m_jumpCooldown;
                    m_rigidBody.AddForce (new Vector2 (0.0f, m_jumpForce), ForceMode2D.Impulse);
                }
            }
        }
        m_jumpCooldownTimer -= Time.fixedDeltaTime;
    }

    void HandleSpinning()
    {
        float horizontal = Input.GetAxis ("Horizontal");
        m_hingeJoint.useMotor = true;
        JointMotor2D motor = m_hingeJoint.motor;
        motor.motorSpeed = horizontal * 90.0f * (Camera.main.orthographicSize / 3.0f);
        m_hingeJoint.motor = motor;
        m_audioSource.volume = Mathf.Abs(horizontal);
    }

    void HandleLegs()
    {
        float vertical = Input.GetAxis ("Vertical");
        float legY = m_distanceJoint.connectedAnchor.y;
        legY += vertical * m_legYChangeSpeed * Time.fixedDeltaTime;
        legY = Mathf.Clamp (legY, m_minLegY, m_maxLegY);
        m_distanceJoint.connectedAnchor = new Vector2(0.0f, legY);
    }

	void FixedUpdate()
	{
		bool locked = Input.GetButton ("Fire1");
		if (locked)
        {
			if (!m_wasLocked)
            {
				Debug.Log ("Locking");
				m_eyebrows.SetActive (true);
                List<Connectable> closestConnectables = FindClosestConnectables ();

				if (closestConnectables.Count == 1)
				{
					float lockingDistance = m_myConnectable.m_lockingRadius + closestConnectables[0].m_lockingRadius;
                    Vector2 fromConnectable = new Vector2(transform.position.x, transform.position.y) - closestConnectables[0].GetAnchorPosition();
                    Vector2 lockPosition = closestConnectables[0].GetAnchorPosition() + fromConnectable.normalized * lockingDistance;
					transform.position = lockPosition;
				}
                else if (closestConnectables.Count == 2)
                {
                    transform.position = GetLockingPosition (closestConnectables);
                }

                if (closestConnectables.Count >= 1 && closestConnectables.Count <= 2)
                {
                    m_relativeAnchor = new Vector2(transform.position.x, transform.position.y) - closestConnectables [0].GetAnchorPosition ();
                }
				m_hingeJoint.enabled = true;
                //m_closestConnectables.Clear();
			}

            if (m_closestConnectables.Count >= 1 && m_closestConnectables.Count <= 2)
            {
                Vector2 lockPosition = m_closestConnectables [0].GetAnchorPosition () + m_relativeAnchor;
                transform.position = lockPosition;
            }

            if (m_inventory.HasItem (Inventory.Item.TurnPower))
            {
                HandleSpinning ();
            }

			m_wasLocked = true;
		}
        else
        {
            FindClosestConnectables ();
			float horizontal = Input.GetAxis ("Horizontal");

            Vector2 movement = new Vector2 (horizontal, 0.0f);//, vertical);
			if (movement.sqrMagnitude > 1.0f)
            {
				movement = movement.normalized;
			}

			Vector2 movementTotal = movement * m_movementForce;
			//transform.position += new Vector3(movementTotal.x, movementTotal.y);
            m_rigidBody.AddForce (movementTotal, ForceMode2D.Force);

            if (m_inventory.HasItem (Inventory.Item.JumpPower))
            {
                HandleJumping ();
            }

            if (m_inventory.HasItem(Inventory.Item.AdjustableLegsPower))
            {
                HandleLegs ();
            }

			m_eyebrows.SetActive (false);
			m_hingeJoint.enabled = false;
			m_wasLocked = false;
            m_audioSource.volume = 0.0f;
		}
	}
}
