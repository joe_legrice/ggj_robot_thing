﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public enum Item
    {
        TurnPower,
        JumpPower,
        AdjustableLegsPower,
        SuperStrength,
        Count
    }

    bool[] m_items;
    public bool m_giveAll;

    void Start()
    {
        m_items = new bool[(int)Item.Count];
        if (m_giveAll)
        {
            for (int i = 0; i < m_items.Length; i++)
            {
                m_items [i] = true;
            }
        }
    }

    public void AddItem(Item item)
    {
        m_items [(int)item] = true;
    }

    public bool HasItem(Item item)
    {
        return m_items [(int)item];
    }
}
