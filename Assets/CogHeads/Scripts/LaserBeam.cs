﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserBeam : MonoBehaviour
{
    LineRenderer m_line;
    int m_layerMask;
    ParticleSystem m_particles;
    float m_startLineWidth;
    public float m_pulsationFrequency = 8.0f;
    public float m_pulsationScale = 0.25f;
    public float m_onTime = 1.0f;
    public float m_onTimeTimer;
    public float m_offTime = 0.0f;
    public float m_offTimeTimer;
    AudioSource m_audioSource;

    void Start()
    {
        m_line = GetComponentInChildren<LineRenderer> ();
        m_startLineWidth = m_line.widthMultiplier;
        m_particles = GetComponentInChildren<ParticleSystem> ();
        m_layerMask = LayerMask.GetMask ("Default", "PlayerCog", "Mechanisms");
        m_audioSource = GetComponent<AudioSource> ();
    }

    void Update()
    {
        bool isOn = false;
        if (m_onTimeTimer > 0.0f)
        {
            m_onTimeTimer -= Time.deltaTime;
            isOn = true;
            if (m_onTimeTimer <= 0.0f)
            {
                m_offTimeTimer = m_offTime;
            }
        }
        else
        {
            m_offTimeTimer -= Time.deltaTime;
            if (m_offTimeTimer <= 0.0f)
            {
                m_onTimeTimer = m_onTime;
            }
        }

        if (isOn)
        {
            m_line.enabled = true;
            ParticleSystem.EmissionModule emission = m_particles.emission;
            emission.enabled = true;
            Fire ();
        }
        else
        {
            m_line.enabled = false;
            ParticleSystem.EmissionModule emission = m_particles.emission;
            emission.enabled = false;
        }
        m_audioSource.volume = (isOn ? 1.0f : 0.0f);
    }

    void Fire()
    {
        float distanceTravelled = 1000.0f;
        Vector2 fireDirection = transform.right;
        Vector2 fireSource = transform.position;
        fireSource += fireDirection * transform.localScale.x * 0.5f;
        RaycastHit2D hit = Physics2D.Raycast (fireSource, fireDirection, distanceTravelled, m_layerMask);
        if (hit.collider != null)
        {
            distanceTravelled = hit.distance;

            if (hit.rigidbody != null)
            {
                Damagable damagable = hit.rigidbody.GetComponentInChildren<Damagable> ();
                if (damagable != null)
                {
                    damagable.Damage (1);
                }
            }
        }
        Vector2 endPoint = fireSource + fireDirection * distanceTravelled;

        m_line.SetPosition (0, transform.position);
        m_line.SetPosition (1, endPoint);
        m_line.widthMultiplier = m_startLineWidth + m_startLineWidth * Mathf.Sin (Time.time * m_pulsationFrequency) * m_pulsationScale;
        m_particles.transform.position = endPoint;
    }
}
