﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationFollow : MonoBehaviour
{
    public Transform m_followTarget;
    public float m_multiplier = 1.0f;
    public float m_angleOffset = 0.0f;

    float m_lastFollowAngle;

    void Update()
    {
        float currentFollowAngle = m_followTarget.eulerAngles.z;
        float angleDiff = currentFollowAngle - m_lastFollowAngle;
        if (currentFollowAngle < 45 && m_lastFollowAngle > 225)
        {
            // overflowed
            angleDiff += 360;
        }
        else if (currentFollowAngle > 225 && m_lastFollowAngle < 45)
        {
            angleDiff -= 360;
        }
        transform.eulerAngles = new Vector3 (0.0f, 0.0f, transform.eulerAngles.z + angleDiff * m_multiplier);

        m_lastFollowAngle = currentFollowAngle;
        //transform.rotation = Quaternion.SlerpUnclamped (Quaternion.identity, m_followTarget.rotation, m_multiplier);// m_followTarget.rotation //new Vector3 (0.0f, 0.0f, (m_followTarget.eulerAngles.z * m_multiplier) + m_angleOffset);
    }
}
