﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LegAnimator : MonoBehaviour
{
    public GameObject m_leftLeg;
    public GameObject m_rightLeg;
    public DistanceJoint2D m_distanceJoint;

    public float m_animationSpeed = 1.0f;
    public float m_legRotationRange = 45.0f;
    public float m_minLegLength = -1.0f;
    public float m_maxLegLength = 1.0f;
    public float m_legLengthAngleScale = 1.0f;

    float m_phase = 0.0f;

    float m_direction = 1.0f;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        float horizontal = Input.GetAxis ("Horizontal");

        float legLength = (m_distanceJoint.connectedAnchor.y - m_minLegLength);
        float legShortness = m_maxLegLength - m_distanceJoint.connectedAnchor.y;
        m_phase += m_animationSpeed * horizontal * legShortness;
        float rotateAmount = m_legRotationRange * Mathf.Abs (horizontal) * legLength * m_legLengthAngleScale;
        m_leftLeg.transform.localEulerAngles = new Vector3 (0.0f, 0.0f, Mathf.Sin(m_phase) * rotateAmount);
        m_rightLeg.transform.localEulerAngles = new Vector3 (0.0f, 0.0f, -Mathf.Sin(m_phase + (Mathf.PI * 0.25f) * horizontal) * rotateAmount);

        if (horizontal > 0.0f)
        {
            m_direction = 1.0f;
        }
        else if (horizontal < 0.0f)
        {
            m_direction = -1.0f;
        }
        transform.localScale = new Vector3(m_direction, 1.0f, 1.0f);
	}
}
