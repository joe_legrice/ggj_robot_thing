﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SinRotationFollow : MonoBehaviour {

    public Transform m_followTarget;
    public float m_maxRotation;

    void LateUpdate()
    {
        float currentRotation = m_followTarget.eulerAngles.z;

        float sin = Mathf.Sin (currentRotation * Mathf.Deg2Rad);
        transform.eulerAngles = new Vector3 (0.0f, 0.0f, sin * m_maxRotation);
    }
}
