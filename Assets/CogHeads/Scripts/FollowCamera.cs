﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour {

    public Transform m_target;
    public float m_followSpeed = 1.0f;
    public float m_zoomSpeed = 1.0f;
    public float m_targetZoom = 3.0f;
    float m_startZ;

    Camera m_camera;
	// Use this for initialization
	void Start ()
    {
        m_camera = GetComponent<Camera> ();
        m_startZ = transform.position.z;
        transform.position = new Vector3 (m_target.position.x, m_target.position.y, m_startZ);
        m_camera.orthographicSize = m_targetZoom;
	}
	
	// Update is called once per frame
	void LateUpdate ()
    {
        transform.position = Vector3.MoveTowards(transform.position, new Vector3 (m_target.position.x, m_target.position.y, m_startZ), m_followSpeed * Time.deltaTime);
        m_camera.orthographicSize = Mathf.MoveTowards (m_camera.orthographicSize, m_targetZoom, m_zoomSpeed * Time.deltaTime);
	}
}
