﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinInstructionsDisplay : MonoBehaviour
{
    public PlayerMovement m_target;
    public Inventory m_playerInventory;
    public GameObject m_textObject;

    void Update()
    {
        bool showInfo = m_target.IsLocked() && m_playerInventory.HasItem(Inventory.Item.TurnPower);
        if (showInfo != m_textObject.activeSelf)
        {
            m_textObject.SetActive (showInfo);
        }
    }
}
