﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Connectable : MonoBehaviour
{
	public float m_lockingRadius = 1.0f;

    HingeJoint2D m_hingeJoint;

    void Start()
    {
        m_hingeJoint = GetComponent<HingeJoint2D> ();
    }

    public Vector2 GetAnchorPosition()
    {
        return m_hingeJoint.connectedAnchor;
    }
}
