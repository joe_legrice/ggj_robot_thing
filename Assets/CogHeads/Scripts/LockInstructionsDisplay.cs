﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockInstructionsDisplay : MonoBehaviour
{
    public PlayerMovement m_target;
    public GameObject m_textObject;
    public GameObject m_tick1;
    public GameObject m_tick2;

    void Update()
    {
        List<Connectable> closestConnectables = m_target.GetClosestConnectables ();
        bool showInfo = closestConnectables.Count > 0 && !m_target.IsLocked();
        if (showInfo != m_textObject.activeSelf)
        {
            m_textObject.SetActive (showInfo);
        }
        if (closestConnectables.Count >= 1)
        {
            m_tick1.SetActive (true);
            m_tick1.transform.position = closestConnectables [0].transform.position;
        }
        else
        {
            m_tick1.SetActive (false);
        }
        if (closestConnectables.Count >= 2)
        {
            m_tick2.SetActive (true);
            m_tick2.transform.position = closestConnectables [1].transform.position;
        }
        else
        {
            m_tick2.SetActive (false);
        }
    }
}
