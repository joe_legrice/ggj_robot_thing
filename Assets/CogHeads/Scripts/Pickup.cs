﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour
{
    public Inventory.Item m_item;
    public TextMesh m_text;
    Inventory m_playerInventory;
    ParticleSystem m_particleSystem;
    AudioSource m_audioSource;

    void Start()
    {
        m_playerInventory = FindObjectOfType<Inventory> ();
        m_particleSystem = GetComponent<ParticleSystem> ();
        m_text.color = new Color (1.0f, 1.0f, 1.0f, 0.0f);
        m_audioSource = GetComponent<AudioSource> ();
    }

    void Update()
    {
        if (!m_particleSystem.emission.enabled)
        {
            float newAlpha = Mathf.Min (1.0f, m_text.color.a + Time.deltaTime);
            m_text.color = new Color(1.0f, 1.0f, 1.0f, newAlpha);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (m_particleSystem.emission.enabled)
        {
            Debug.Log ("Pickup Triggered!");
            ParticleSystem.EmissionModule emission = m_particleSystem.emission;
            emission.enabled = false;
            m_playerInventory.AddItem (m_item);
            m_audioSource.Play();
        }
    }
}
