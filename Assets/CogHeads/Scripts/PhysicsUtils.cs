﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsUtils : MonoBehaviour
{
    public class Circle
    {
        public Vector2 p;
        public float radius;
    }
    public static List<Vector2> Intersect(Circle c1, Circle c2)
    {
        List<Vector2> rLp = new List<Vector2>();
        float d = new Vector2(c1.p.x - c2.p.x, c1.p.y - c2.p.y).magnitude;

        if (d > (c1.radius + c2.radius))
            return rLp;
        else if (d == 0 && c1.radius == c2.radius)
            return rLp;
        else if ((d + Mathf.Min(c1.radius, c2.radius)) < Mathf.Max(c1.radius, c2.radius))
            return rLp;
        else
        {
            float a = (c1.radius * c1.radius - c2.radius * c2.radius + d * d) / (2 * d);
            float h = Mathf.Sqrt(c1.radius * c1.radius - a * a);

            //Vector2 p2 = new Vector2((float)(c1.p.X + (a * (c2.p.X - c1.p.X)) / d), (float)(c1.p.Y + (a * c2.p.Y - c1.p.Y) / d));
            Vector2 p2 = new Vector2((float)(c1.p.x + (a * (c2.p.x - c1.p.x)) / d),
                (float)(c1.p.y + (a * (c2.p.y - c1.p.y)) / d));

            Vector2 i1 = new Vector2((float)(p2.x + (h * (c2.p.y - c1.p.y)) / d), (float)(p2.y - (h * (c2.p.x - c1.p.x)) / d));
            Vector2 i2 = new Vector2((float)(p2.x - (h * (c2.p.y - c1.p.y)) / d), (float)(p2.y + (h * (c2.p.x - c1.p.x)) / d));

            if (d == (c1.radius + c2.radius))
                rLp.Add(i1);
            else
            {
                rLp.Add(i1);
                rLp.Add(i2);
            }

            return rLp;
        }
    }
}
