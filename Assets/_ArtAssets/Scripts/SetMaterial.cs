﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetMaterial : MonoBehaviour 
{
	public string thename = "_FireCenter";
	public Material[] materials;
	public Transform theobject;

	private Vector4 vector = Vector4.zero;

	void Update () 
	{
		vector.x = theobject.position.x;
		vector.y = theobject.position.y;
		vector.z = theobject.position.z;

		for(int i=0; i<materials.Length;i++)
		{
			materials[i].SetVector(thename,vector);	
		}
	}
}
