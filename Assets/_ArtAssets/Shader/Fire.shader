﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "GGJ/Fire"
{
Properties 
{
	[NoScaleOffset] _MainTex ("_MainTex RGBA", 2D) = "white" {}

	[Header(BlendModes)]
	//[Enum(UnityEngine.Rendering.BlendMode)] _SrcBlend ("Source", Int) = 1.0
	[Enum(Additive,1,AlphaBlend,10)] _DstBlend ("Desination Blend Options", Int) = 0
	//[Enum(UnityEngine.Rendering.BlendOp)] _BlendOpFactor ("Blend Operation", Int) = 0

	[Header(UseInputColor (SELECT ONLY 1))]
	[Toggle(INPUT_COLOR_CHANGE)] _UseChangeColor ("Change Color", Float) = 1
	[Toggle(INPUT_COLOR_MULTIPLY)] _UseInputColorMul ("Multiply Color", Float) = 1
	[Toggle(INPUT_COLOR_ADDITIVE)] _UseInputColorAdd ("Additive Color", Float) = 0
	[Toggle(INPUT_COLOR_BLEND)] _UseInputColorBld ("Blend Color", Float) = 0

	[Header(Distortion)]
	_Distort("Distort Strength", Range(0,1)) = 1
	_DistortSpeed("Distort Speed", Range(0,1)) = 1
	_DistortTex("Distort", 2D) = "black" {}
}

Category 
{
	Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
	Blend SrcAlpha [_DstBlend]
	//BlendOp [_BlendOpFactor]
	Cull Off Lighting Off ZWrite Off
	Lighting Off
	
	SubShader 
	{
		Pass 
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#pragma multi_compile __ UNITY_UI_ALPHACLIP

			#pragma multi_compile __ INPUT_COLOR_CHANGE
			#pragma multi_compile __ INPUT_COLOR_MULTIPLY
			#pragma multi_compile __ INPUT_COLOR_ADDITIVE
			#pragma multi_compile __ INPUT_COLOR_BLEND

			#include "UnityCG.cginc"

			sampler2D _MainTex;
			sampler2D _DistortTex;
			half4 _DistortTex_ST;
			fixed _Distort;
			fixed _DistortSpeed;

			struct appdata_t 
			{
				fixed4 vertex : POSITION;
				fixed2 texcoord : TEXCOORD0;
				fixed4 color : COLOR;
			};

			struct v2f 
			{
				fixed4 vertex : SV_POSITION;
				fixed2 texcoord : TEXCOORD0;
				fixed4 color : COLOR;
			};

			v2f vert (appdata_t v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.texcoord =  v.texcoord;
				o.color = v.color;

				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				float2 distortUV = i.texcoord.xy * _DistortTex_ST.xy + _DistortTex_ST.zw;
				distortUV += (_Time.y*_DistortSpeed);

				fixed2 distort = tex2D(_DistortTex, distortUV).rg;

				float2 uv = i.texcoord.xy;
				uv += ((distort - 0.5f)*2.0f)*_Distort;

				fixed4 color = tex2D(_MainTex, uv).r;

				//==========================================
				//fixed4 color = tex2D(_MainTex,i.texcoord);

				//Color Change
				#ifdef INPUT_COLOR_CHANGE
					color.rgb = lerp(1,i.color.rgb,color.r);
				#endif

				//Multiply
				#ifdef INPUT_COLOR_MULTIPLY
					color.rgb *= i.color.rgb;
				#endif

				//Additive
				#ifdef INPUT_COLOR_ADDITIVE
					color.rgb += i.color.rgb;
				#endif

				//Blend
				#ifdef INPUT_COLOR_BLEND
					color.rgb = lerp(color.rgb,i.color.rgb,i.color.a);
				#endif
				//

				color.a *= i.color.a;

				return color;
			}
			ENDCG 
		}
	}	
}
}
