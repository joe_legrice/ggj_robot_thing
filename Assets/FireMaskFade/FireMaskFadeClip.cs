using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class FireMaskFadeClip : PlayableAsset, ITimelineClipAsset
{
    public FireMaskFadeBehaviour template = new FireMaskFadeBehaviour ();

    public ClipCaps clipCaps
    {
        get { return ClipCaps.Blending; }
    }

    public override Playable CreatePlayable (PlayableGraph graph, GameObject owner)
    {
        var playable = ScriptPlayable<FireMaskFadeBehaviour>.Create (graph, template);
        return playable;
    }
}
