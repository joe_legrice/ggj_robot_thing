using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class FireMaskFadeBehaviour : PlayableBehaviour
{
    public bool enabled = true;
}
