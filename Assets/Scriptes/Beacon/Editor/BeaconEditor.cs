﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Beacon))]
public class BeaconEditor : Editor
{
    public Beacon Target { get { return (Beacon)target; } }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        serializedObject.Update();
        SerializedProperty radius = serializedObject.FindProperty("m_radius");
        SerializedProperty rangeTransform = serializedObject.FindProperty("m_rangeTransform");
        if (rangeTransform.objectReferenceValue != null)
        {
            Transform t = (Transform)rangeTransform.objectReferenceValue;
            t.localScale = Vector3.one * radius.floatValue * 2;
        }
        serializedObject.ApplyModifiedProperties();
    }

    private void OnSceneGUI()
    {
        Handles.DrawWireDisc(Target.transform.position, Target.transform.up, Target.Range);
    }
}
