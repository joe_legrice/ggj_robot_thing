﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Beacon : MonoBehaviour
{
    private static HashSet<Beacon> s_allBeacons = new HashSet<Beacon>();
    public static HashSet<Beacon> Instances { get { return s_allBeacons; } }

    [SerializeField] private float m_radius;
    [SerializeField] private Collider m_collider;
    [SerializeField] private Transform m_rangeTransform;
    [SerializeField] private MeshRenderer m_meshRenderer;
    [SerializeField] [ColorUsage(true, true, 0.0f, 8.0f, 0.125f, 3.0f)] private Color m_enabledColor;
    [SerializeField] [ColorUsage(true, true, 0.0f, 8.0f, 0.125f, 3.0f)] private Color m_disabledColor;
    [SerializeField] private AudioClip m_onSounds;
    [SerializeField] private AudioClip m_offSounds;

    private AudioSource audioSource;
    private static float s_minDistance = 1.0f;
    private bool m_isActive;

    public float Range { get { return m_radius; } }



    public bool IsActive()
    {
        return m_isActive;
    }

    public bool IsInRange(Vector3 position)
    {
        Vector3 beaconTransform = position;
        beaconTransform.y = 0;
        Vector3 robotPosition = transform.position;
        robotPosition.y = 0;
        float distance = Vector3.Distance(beaconTransform, robotPosition);

        return distance <= m_radius;
    }

    public bool HasArrived(Vector3 position)
    {
        Vector3 beaconTransform = position;
        beaconTransform.y = 0;
        Vector3 robotPosition = transform.position;
        robotPosition.y = 0;
        float distance = Vector3.Distance(beaconTransform, robotPosition);

        return distance <= s_minDistance;
    }

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        s_allBeacons.Add(this);
        UpdateActiveState();
    }

    private void OnDestroy()
    {
        s_allBeacons.Remove(this);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hitInfo;
            Ray clickRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (m_collider.Raycast(clickRay, out hitInfo, float.MaxValue))
            {
                m_isActive = !m_isActive;
                UpdateActiveState();
                //attach audio to source depends on whether the Beacon is on or off
                audioSource.clip = m_isActive ? m_onSounds : m_offSounds;
                //play audio
                audioSource.Play();
            }
        }
    }

    private void UpdateActiveState()
    {
        m_rangeTransform.gameObject.SetActive(m_isActive);
        m_meshRenderer.material.SetColor("_EmissionColor", m_isActive ? m_enabledColor : m_disabledColor);
    }
}
