﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Conveyor : MonoBehaviour
{
    [SerializeField] private float m_conveyorSpeed;

    public Vector3 Speed { get { return m_conveyorSpeed * transform.forward; } }
}
