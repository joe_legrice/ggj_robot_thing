﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Level : MonoBehaviour
{
    [SerializeField] private Text m_levelIndexText;
    [SerializeField] private Text m_levelNameText;
    [SerializeField] private Button m_button;

    private LevelData.LevelInfo m_levelInfo;
    
    public void SetInfo(int levelNumber, LevelData.LevelInfo li)
    {
        m_levelIndexText.text = levelNumber.ToString();
        m_levelNameText.text = li.m_levelName;
        m_levelInfo = li;
    }

    private void Start()
    {
        m_button.onClick.AddListener(DoClick);
    }

    private void DoClick()
    {
        LevelManager.Instance.LoadLevel(m_levelInfo);
    }
}
