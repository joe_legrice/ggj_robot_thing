﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SavedXRobots : MonoBehaviour {
    [SerializeField] private Text m_text;
    [SerializeField] private string m_formatString = "You saved {0} robots!";

    private void OnEnable()
    {
        m_text.text = string.Format(m_formatString, Goal.Instance.NumberOfRobotsSaved);
    }
}
