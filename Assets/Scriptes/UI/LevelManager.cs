﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoSingleton<LevelManager>
{
    [SerializeField] private LevelData m_levelData;

    private LevelData.LevelInfo m_currentLevel;

    public LevelData GetLevelData()
    {
        return m_levelData;
    }

    public LevelData.LevelInfo GetCurrentLevel()
    {
        return m_currentLevel;
    }

    public void RestartLevel()
    {
        if (m_currentLevel != null)
        {
            LoadLevel(m_currentLevel);
        }
    }

    public void ExitLevel()
    {
        StartCoroutine(DoLoad(null));
    }

    public void LoadLevel(LevelData.LevelInfo li)
    {
        StartCoroutine(DoLoad(li));
    }

    public void TryGoToNextLevel()
    {
        if (m_currentLevel != null)
        {
            int nextLevelIndex = m_levelData.GetLevelIndex(m_currentLevel) + 1;

            if (nextLevelIndex < m_levelData.NumberOfLevels)
            {
                LevelData.LevelInfo nextLi = m_levelData.GetLevelInfo(nextLevelIndex);
                LoadLevel(nextLi);
            }
            else
            {
                ExitLevel();
                ScreenManager.Instance.SetScreen(ScreenManager.ScreenType.MenuScreen);
            }
        }
    }

    private IEnumerator DoLoad(LevelData.LevelInfo li)
    {
        if (m_currentLevel != null)
        {
            yield return SceneManager.UnloadSceneAsync(m_currentLevel.m_levelSceneName);
        }
        m_currentLevel = li;

        if (m_currentLevel != null)
        {
            yield return SceneManager.LoadSceneAsync(m_currentLevel.m_levelSceneName);
            ScreenManager.Instance.SetScreen(m_currentLevel.m_isCogScene ?
                ScreenManager.ScreenType.InCogGameScreen : ScreenManager.ScreenType.InGameScreen);
        }
    }
}
