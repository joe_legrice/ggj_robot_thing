﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class PopupManager : MonoSingleton<PopupManager>
{
    public enum PopupType
    {
        PausePopup,
        WinPopup,
        LosePopup
    }

    [System.Serializable]
    public class PopupSetup
    {
        public PopupType m_panelType;
        public GameObject m_gameObject;
    }

    [SerializeField] private PopupSetup[] m_popupConfig;

    private PopupSetup m_activePopupSetup;

    public void ShowPopup(PopupType p)
    {
        if (m_activePopupSetup != null && m_activePopupSetup.m_panelType != p)
        {
            m_activePopupSetup.m_gameObject.SetActive(false);
            m_activePopupSetup = null;
        }

        if (m_activePopupSetup == null)
        {
            foreach (PopupSetup ps in m_popupConfig)
            {
                if (ps.m_panelType == p)
                {
                    ps.m_gameObject.SetActive(true);
                    m_activePopupSetup = ps;
                }
            }
        }
    }

    public void CloseCurrentPopup()
    {
        if (m_activePopupSetup != null)
        {
            m_activePopupSetup.m_gameObject.SetActive(false);
            m_activePopupSetup = null;
        }
    }

    private void Start()
    {
        foreach (PopupSetup ps in m_popupConfig)
        {
            ps.m_gameObject.SetActive(false);
        }
    }
}
