﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGamePanel : MonoSingleton<InGamePanel>
{
    [SerializeField] private Text m_text;
    [SerializeField] private Text m_missionText;
    [SerializeField] private GameObject m_completeHierarcy;
    [SerializeField] private string m_formatString = "Mission : Save {0} Robots!";

    public void Update()
    {
        m_text.text = string.Format("{0} / {1}", Goal.Instance.NumberOfRobotsSaved, Robot.TotalNumberOfRobots);
        m_missionText.text = string.Format(m_formatString, Goal.Instance.NumberOfRobotsNeeded);
        m_completeHierarcy.SetActive(Goal.Instance.NumberOfRobotsSaved >= Goal.Instance.NumberOfRobotsNeeded);
    }
}
