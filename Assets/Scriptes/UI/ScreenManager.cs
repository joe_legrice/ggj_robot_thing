﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class ScreenManager : MonoSingleton<ScreenManager>
{
    public enum ScreenType
    {        
        OptionsScreen,
        MenuScreen,
        InGameScreen,
        LevelSelectScreen,
        InCogGameScreen,
        MinigameIntroScene,
        EndGameCutscene,
        PreGameCutscene
    }

    [System.Serializable]
    public class ScreenSetup
    {
        public ScreenType m_panelType;
        public GameObject m_gameObject;
    }

    [SerializeField] private ScreenSetup[] m_screenConfig;
    [SerializeField] private ScreenType m_defaultScreen;

    private ScreenSetup m_activeScreen;

    public void SetScreen(ScreenType p)
    {
        if (m_activeScreen != null && m_activeScreen.m_panelType != p)
        {
            m_activeScreen.m_gameObject.SetActive(false);
            m_activeScreen = null;
        }

        if (m_activeScreen == null)
        {
            foreach (ScreenSetup ps in m_screenConfig)
            {
                if (ps.m_panelType == p)
                {
                    ps.m_gameObject.SetActive(true);
                    m_activeScreen = ps;
                }
            }
        }
    }

    private void Start()
    {
        foreach (ScreenSetup ps in m_screenConfig)
        {
            ps.m_gameObject.SetActive(false);
        }
        SetScreen(m_defaultScreen);
    }
}
