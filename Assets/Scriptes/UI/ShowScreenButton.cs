﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowScreenButton : MonoBehaviour
{
    [SerializeField] private ScreenManager.ScreenType m_screenType;
    [SerializeField] private Button m_button;

    private void Start()
    {
        m_button.onClick.AddListener(DoClick);
    }

    private void DoClick()
    {
        ScreenManager.Instance.SetScreen(m_screenType);
    }
}
