﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoToNextLevelAfterDelay : MonoBehaviour
{
    [SerializeField] private float m_delay;

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(m_delay);
        LevelManager.Instance.TryGoToNextLevel();
    }
}
