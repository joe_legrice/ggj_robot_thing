﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NextLevelButton : MonoBehaviour
{
    [SerializeField] private Button m_button;

    private void Start()
    {
        m_button.onClick.AddListener(DoClick);
    }

    private void DoClick()
    {
        PopupManager.Instance.CloseCurrentPopup();
        LevelManager.Instance.TryGoToNextLevel();
    }
}
