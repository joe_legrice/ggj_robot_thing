﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverPanel : MonoSingleton<GameOverPanel>
{
    public void TryShowEndLevel()
    {
        if (Robot.NumberOfRobotsAlive == 0)
        {
            PopupManager.Instance.ShowPopup(Goal.Instance.NumberOfRobotsSaved >= Goal.Instance.NumberOfRobotsNeeded ? 
                PopupManager.PopupType.WinPopup : PopupManager.PopupType.LosePopup);
        }
    }
}
