﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelectScreen : MonoBehaviour
{
    [SerializeField] private Transform m_spawnTransform;
    [SerializeField] private GameObject m_levelPrefab;

    private void Start()
    {
        LevelData levelData = LevelManager.Instance.GetLevelData();
        for (int i = 0; i < levelData.NumberOfLevels; i++)
        {
            LevelData.LevelInfo li = levelData.GetLevelInfo(i);
            GameObject levelInstance = GameObject.Instantiate(m_levelPrefab);
            levelInstance.transform.SetParent(m_spawnTransform, false);
            Level l = levelInstance.GetComponent<Level>();
            l.SetInfo(i + 1, li);
        }
    }

}
