﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuButton : MonoBehaviour
{
    [SerializeField] private Button m_button;

    private void Start()
    {
        m_button.onClick.AddListener(DoClick);
    }

    private void DoClick()
    {
        LevelManager.Instance.ExitLevel();
        ScreenManager.Instance.SetScreen(ScreenManager.ScreenType.MenuScreen);
        PopupManager.Instance.CloseCurrentPopup();
    }
}
