﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Robot : MonoBehaviour
{
    [SerializeField] private Animator m_animator;
    [SerializeField] private Rigidbody m_rigidbody;
    [SerializeField] private float m_speed;
    [SerializeField] private float m_lookRotateSpeed = 5.0f;
    [SerializeField] private float m_talkFrequency = 2f;

    [SerializeField] private AudioClip m_walkingSound;
    [SerializeField] private AudioClip m_dieSound;
    [SerializeField] private AudioClip m_teleportSound;
    //randomly selecting one of the clip from the array to play when confused
    [SerializeField] private AudioClip[] m_confusedSound;
    AudioSource audioSource;

    private static int s_totalRobots = 0;
    private static int s_numberOfRobots = 0;
    public static int TotalNumberOfRobots { get { return s_totalRobots; } }
    public static int NumberOfRobotsAlive { get { return s_numberOfRobots; } }

    private List<Conveyor> m_conveyor = new List<Conveyor>();
    private bool m_reachedGoal;
    private bool m_wasKilled;
    private float timer;

    private bool TryGetCurrentBeacon(ref Vector3 position)
    {
        int numBeacons = 0;
        bool foundValidBeacon = false;
        foreach (Beacon b in Beacon.Instances)
        {
            if (b.IsActive() && b.IsInRange(transform.position))
            {
                numBeacons++;
                if (!b.HasArrived(transform.position))
                {
                    foundValidBeacon = true;
                    position = b.transform.position;
                }
            }
        }

        m_animator.SetBool("IsConfused", numBeacons > 1);
        m_animator.SetBool("IsWalking", numBeacons >= 1);

        //when confused, play sound if it is not already playing
        if(numBeacons > 1)
        {
            // the robot might not talk right after m_talkFrequency
            float chanceOfTalking = Random.Range(0f, 1f);
            if (timer > m_talkFrequency && chanceOfTalking > 0.5f)
            {
                timer = 0;
                //choose a random clip to play
                int clipIndex = Random.Range(0, m_confusedSound.Length);
                audioSource.clip = m_confusedSound[clipIndex];
                audioSource.Play();
            }
            else if(!audioSource.isPlaying)
            {
                timer += Time.fixedDeltaTime;
            }
        }
        return numBeacons == 1 && foundValidBeacon;
    }

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        s_totalRobots = 0;
        s_numberOfRobots = 0;
    }

    private void Start()
    {
        s_totalRobots++;
        s_numberOfRobots++;
    }

    private void FixedUpdate()
    {
        Vector3 targetPosition = Vector3.zero;
        if (!m_reachedGoal && !m_wasKilled && TryGetCurrentBeacon(ref targetPosition) && !m_reachedGoal)
        {
            Vector3 lookDirection = targetPosition - transform.position;
            lookDirection.y = 0;
            lookDirection.Normalize();

            Quaternion newRotation = Quaternion.LookRotation(lookDirection, Vector3.up);
            transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, m_lookRotateSpeed * Time.fixedDeltaTime);

            m_rigidbody.velocity = m_speed * lookDirection;

            //play walking sound if it is not playing
            if (!audioSource.isPlaying)
            {
                //if it is walking, play walking SE
                audioSource.clip = m_walkingSound;
                //the clip start at approx. this time
                audioSource.time = 0.2f;
                audioSource.loop = true;
                audioSource.Play();
            }
        }
        else
        {
            m_rigidbody.velocity = Vector3.zero;
            //stop walking audio when velocity is 0
            if (audioSource.clip == m_walkingSound)
            {
                audioSource.Stop();
                audioSource.loop = false;
            }
        }

        foreach (Conveyor c in m_conveyor)
        {
            m_rigidbody.velocity += c.Speed;
        }
    }

    public void GetKilled()
    {
        if (!m_reachedGoal && !m_wasKilled)
        {
            m_wasKilled = true;

            audioSource.clip = m_dieSound;
            audioSource.loop = false;
            audioSource.Play();
        
            m_animator.SetTrigger("Explode");
        }
    }

    public void CleanupAfterDeath()
    {
        s_numberOfRobots--;
        if (s_numberOfRobots == 0)
        {
            GameOverPanel.Instance.TryShowEndLevel();
        }
        Destroy(gameObject);
    }

    public bool SetInGoal()
    {
        if (!m_reachedGoal && !m_wasKilled)
        {
            audioSource.clip = m_teleportSound;
            audioSource.loop = false;
            audioSource.Play();
            m_reachedGoal = true;
            m_animator.SetTrigger("ReachedGoal");

            return true;
        }
        else
        {
            return false;
        }
    }

    private void OnTriggerEnter(Collider c)
    {
        Conveyor con = c.GetComponent<Conveyor>();
        if (con != null)
        {
            m_conveyor.Add(con);
        }
    }

    private void OnTriggerExit(Collider c)
    {
        Conveyor con = c.GetComponent<Conveyor>();
        if (con != null)
        {
            m_conveyor.Remove(con);
        }
    }
}
