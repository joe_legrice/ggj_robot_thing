﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControler : MonoBehaviour
{
    [SerializeField] float offSetFromEdge = 0.01f; // by percentage
    [SerializeField] float speed = 5f;
    [SerializeField] float maxZoom = 10;
    [SerializeField] float minZoom = 60;
    [SerializeField] float zoomSpeed = 3f;
    Camera fireCamera;
    Transform background;
    //float vert;
    //float horz;
    float left = 0;
    float bottom = 0;
    float screenHeight;
    float screenWidth;
    //bounds of camera
    //float leftBound;
    //float rightBound;
    //float topBound;
    //float botBound;

    private void Awake()
    {
        screenHeight = Screen.height;
        screenWidth = Screen.width;
        //background = GameObject.FindGameObjectWithTag("Base").transform;
        fireCamera = transform.Find("FireCamera").GetComponent<Camera>();
        UpdateBound();
    }

    void UpdateBound()
    {
        //float dist = Vector3.Distance(transform.position, background.position);
        //vert = 2.0f * dist * Mathf.Tan(Camera.main.fieldOfView * 0.5f * Mathf.Deg2Rad) / 2;
        //horz = vert * Camera.main.aspect / 2;
        //MeshRenderer renderer = background.GetComponent<MeshRenderer>();
        //leftBound = horz - renderer.bounds.size.x / 2;
        //rightBound = renderer.bounds.size.x / 2 - horz;
        //botBound = vert - renderer.bounds.size.z / 2;
        //topBound = renderer.bounds.size.z / 2 - vert;
    }

    void Zoom(float zoom)
    {
        //do nothing if user is not zooming
        if (zoom == 0f)
            return;
        Camera.main.fieldOfView = Mathf.Clamp(Camera.main.fieldOfView - zoom * zoomSpeed, maxZoom, minZoom);
        //update sub camera's field of view as well
        fireCamera.fieldOfView = Camera.main.fieldOfView;
        UpdateBound();
    }

    //a very crude and not elegant camera movement control
    void Update()
    {
        Vector3 mousePosInScreen = Input.mousePosition; //redundant
        if (mousePosInScreen.x > 0 && mousePosInScreen.x < screenWidth
            && mousePosInScreen.y > 0 && mousePosInScreen.y < screenHeight)
        {
            int horizontalMove = 0;
            if (mousePosInScreen.x > screenWidth - offSetFromEdge * screenWidth)
            {
                horizontalMove = 1;
            }
            else if (mousePosInScreen.x < offSetFromEdge * screenWidth)
            {
                horizontalMove = -1;
            }

            int verticalMove = 0;
            if (mousePosInScreen.y > screenHeight - offSetFromEdge * screenHeight)
            {
                verticalMove = 1;
            }
            else if (mousePosInScreen.y < offSetFromEdge * screenHeight)
            {
                verticalMove = -1;
            }

            float forwardMove = Input.GetAxis("Mouse ScrollWheel");

            Vector3 oldPos = transform.position;
            Vector3 horDir = transform.right * horizontalMove * speed * Time.deltaTime;
            Vector3 verDir = transform.forward * verticalMove * speed * Time.deltaTime;
            Vector3 forDir = -transform.up * forwardMove * zoomSpeed * Time.deltaTime;
            transform.position += horDir + verDir + forDir;
            //transform.position = new Vector3(Mathf.Clamp(transform.position.x, leftBound, rightBound)
            //, oldPos.y,
            //Mathf.Clamp(transform.position.z, botBound, topBound));
        }
        
    }
}
