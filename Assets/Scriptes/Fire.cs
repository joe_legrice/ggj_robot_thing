﻿using System.Collections;
using UnityEngine;

public class Fire : MonoSingleton<Fire>
{
    private const float c_playerRadius = 1.0f;

    [SerializeField] private ParticleSystem m_fireParticles;
    [SerializeField] private float m_levelTime;
    [SerializeField] private float m_startRadius;
    [SerializeField] private AnimationCurve m_burnRadiusRate;
    [SerializeField] private AnimationCurve m_burnEmissionRate;
    [SerializeField] private SphereCollider m_collider;
    
    [SerializeField] private MeshRenderer m_background;
    [SerializeField] private float m_backgroundRadius;
    [SerializeField] private float m_alarmtime;
    [SerializeField] private AudioClip alarm;
    private AudioSource alarmAudioSource;
    private AudioSource fireAudioSource;

    public float StartRadius { get { return m_startRadius; } }

    float timePassed;

    public void StartBurning()
    {
        StartCoroutine(StartGame());
    }

    private void Awake()
    {
        AudioSource[] audioSources = GetComponents<AudioSource>();
        //the first audio(alarm)
        alarmAudioSource = audioSources[0];
        //the second audio
        fireAudioSource = audioSources[1];
    }

    private void Start()
    {
        StartBurning();        
    }

    private void Update()
    {
        //play different alarm overtime
        if(timePassed > m_levelTime - m_alarmtime && !alarmAudioSource.isPlaying)
        {
            alarmAudioSource.clip = alarm;
            alarmAudioSource.Play();
        }
    }

    private IEnumerator StartGame()
    {
        float initialEmission = m_fireParticles.emission.rateOverTime.constant;
        timePassed = 0.0f;
        while (timePassed < m_levelTime)
        {
            float radiusTime = m_burnRadiusRate.Evaluate(timePassed / m_levelTime);
            float thisRadius = Mathf.Lerp(m_startRadius, 0, radiusTime);
            ParticleSystem.ShapeModule sm = m_fireParticles.shape;
            sm.radius = thisRadius;
            m_collider.radius = thisRadius - c_playerRadius;

            float bgTime = 0.5f * (1.0f - m_burnRadiusRate.Evaluate(timePassed / m_levelTime));
            m_background.material.SetFloat("_Progress", bgTime);

            float et = 1.0f - m_burnEmissionRate.Evaluate(timePassed / m_levelTime);
            float thisEmission = Mathf.Lerp(initialEmission, 0, et);
            ParticleSystem.EmissionModule em = m_fireParticles.emission;
            ParticleSystem.MinMaxCurve mmc = em.rateOverTime;
            mmc.constant = thisEmission;
            em.rateOverTime = mmc;

            timePassed += Time.deltaTime;

            yield return null;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Robot"))
        {
            Robot r = other.GetComponent<Robot>();
            r.GetKilled();
        }
    }
}
