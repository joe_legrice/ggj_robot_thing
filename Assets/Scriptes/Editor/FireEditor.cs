﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Fire))]
public class FireEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        serializedObject.Update();
        float radius = serializedObject.FindProperty("m_startRadius").floatValue;

        MeshRenderer mr = (MeshRenderer)serializedObject.FindProperty("m_background").objectReferenceValue;
        Transform mrt = mr.transform;
        Vector3 curScale = mrt.localScale;
        curScale = Vector3.one * radius * 2;
        mrt.localScale = curScale;

        ParticleSystem ps = (ParticleSystem)serializedObject.FindProperty("m_fireParticles").objectReferenceValue;
        ParticleSystem.ShapeModule sm = ps.shape;
        sm.radius = radius;
        
        serializedObject.ApplyModifiedProperties();
    }

    private void OnSceneGUI()
    {
        Fire f = (Fire)target;
        Handles.DrawWireDisc(f.transform.position, f.transform.up, f.StartRadius);
    }
}
