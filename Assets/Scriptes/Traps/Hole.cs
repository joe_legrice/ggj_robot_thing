﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hole : Trap{
    new Collider collider;

    private void Awake()
    {
        collider = GetComponent<Collider>();
    }

    public override void Switch(bool on)
    {
        Activated = on;
        collider.enabled = Activated;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Robot") && Activated)
        {
            Robot deadRobot = other.GetComponent<Robot>();
            deadRobot.GetKilled();
        }
    }
}
