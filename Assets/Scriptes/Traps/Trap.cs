﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Trap : MonoBehaviour
{
    [SerializeField] bool activated;
    public bool Activated { get { return activated; } set { activated = value; } }
    public abstract void Switch(bool on);
}
