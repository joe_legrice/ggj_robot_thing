﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserBox : Trap
{
    [SerializeField] float rotateSpeed = 2f;
    [SerializeField] AudioClip m_onSounds;
    [SerializeField] AudioClip m_offSounds;
    LineRenderer lineRenderer;
    AudioSource audioSource;
    Vector3 dest;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        lineRenderer = GetComponent<LineRenderer>();
        //always start at itself
        lineRenderer.SetPosition(0, transform.position);
        lineRenderer.enabled = Activated;
    }

    // maybe it can be made in a way that it does not need to be called every frame
    private void Update()
    {
        //if line renderer is enabled than it is active
        if (Activated)
        {
            DrawLine();
            KillRobot();
        }
    }

    private void DrawLine()
    {
        RaycastHit hit;
        //if the laser is blocked by sth
        if (Physics.Raycast(transform.position, transform.forward, out hit, float.MaxValue))
        {
            //setting the end point(blocking obj)
            lineRenderer.SetPosition(1, hit.point);
            dest = hit.point;
        }
        else
        {
            //setting the end point to some where you cant see
            lineRenderer.SetPosition(1, this.transform.forward * 100000);
            dest = transform.forward * 100000;
        }
    }

    //cast a ray and kill any robot on the line
    void KillRobot()
    {
        RaycastHit hit;
        //max distance is the distance between laserBox and where the line ends
        if (Physics.Raycast(transform.position, dest - transform.position, out hit, Vector3.Distance(transform.position, dest)))
        {
            //if it hits a robot
            if (hit.collider.CompareTag("Robot"))
            {
                Robot killedRobot = hit.collider.gameObject.GetComponent<Robot>();
                killedRobot.GetKilled();
            }
        }
    }

    public override void Switch(bool on)
    {
        Activated = on;
        lineRenderer.enabled = Activated;

        //play sound effect
        audioSource.clip = on ? m_onSounds : m_offSounds;
        audioSource.Play();
    }
}
