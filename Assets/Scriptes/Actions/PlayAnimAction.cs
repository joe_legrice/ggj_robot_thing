﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAnimAction : ButtonAction
{
    [SerializeField] Animation anim;
    AudioSource audioSource;
    bool m_played = false;
    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }
    public override void Activate()
    {
        //prevent playing the same clip multiple times
        if (m_played)
            return;
        anim.Play();
        audioSource.Play();
        m_played = true;
    }
    public override void Deactivate()
    {
        //do nothing?
    }
}
