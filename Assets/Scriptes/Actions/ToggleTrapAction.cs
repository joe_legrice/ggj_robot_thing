﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleTrapAction : ButtonAction {

    [SerializeField] Trap linkedTrap;
    
    public override void Activate()
    {
        linkedTrap.Switch(!linkedTrap.Activated);
    }

    public override void Deactivate()
    {
    }
}
