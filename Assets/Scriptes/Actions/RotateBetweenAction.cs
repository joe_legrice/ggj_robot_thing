﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//public class RotateBetween_Old : ButtonAction
//{

//    [SerializeField] private Transform transformToRotate;
//    //interpolating between start and end angle
//    [SerializeField] private float speed;
//    [SerializeField] private float startAngle;
//    [SerializeField] private float endAngle;
//    //direction of rotation
//    bool toEnd = true;
//    float accumTime = 0;

//    public override void Do()
//    {
//        accumTime += Time.deltaTime * (toEnd? 1 : -1);
//        float angle = Mathf.LerpAngle(startAngle, endAngle, accumTime);
//        transformToRotate.eulerAngles = new Vector3(0, angle, 0);
//        if(angle == startAngle)
//        {
//            toEnd = true;
//        }else if(angle == endAngle)
//        {
//            Debug.Log("changing to false");
//            toEnd = false;
//        }
//    }
//}

public class RotateBetweenAction : ButtonAction
{

    [SerializeField] private Transform transformToRotate;
    [SerializeField] private float m_rotateTime;
    [SerializeField] private float startAngle;
    [SerializeField] private float endAngle;

    private bool m_direction;
    private bool m_isRotating;

    public override void Activate()
    {
        if (!m_isRotating)
        {
            m_isRotating = true;
            StartCoroutine(DoRotation());
        }
    }

    public override void Deactivate()
    {
    }

    private IEnumerator DoRotation()
    {
        float initialAngle = m_direction ? endAngle : startAngle;
        float targetAngle = m_direction ? startAngle : endAngle;

        float accumTime = 0;
        while (accumTime < m_rotateTime)
        {
            accumTime += Time.deltaTime;
            
            float angle = Mathf.LerpAngle(initialAngle, targetAngle, accumTime / m_rotateTime);
            transformToRotate.eulerAngles = new Vector3(0, angle, 0);

            yield return null;
        }
        transformToRotate.eulerAngles = new Vector3(0, targetAngle, 0);

        m_direction = !m_direction;
        m_isRotating = false;
    }
}
