﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAction : ButtonAction
{
    [SerializeField] private Transform transformToRotate;
    [SerializeField] private float angle;
    [SerializeField] bool clockWise;
    
    public override void Activate()
    {
        transformToRotate.Rotate(Vector3.up * angle * (clockWise ? 1 : -1));
    }

    public override void Deactivate()
    {
    }
}
