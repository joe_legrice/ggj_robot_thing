﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger : MonoBehaviour
{
    [SerializeField]
    private List<ButtonAction> actions = new List<ButtonAction>();

    private void OnTriggerEnter(Collider other)
    {
        foreach (ButtonAction action in actions)
        {
            action.Activate();
        }

    }

    private void OnTriggerExit(Collider other)
    {
        foreach (ButtonAction action in actions)
        {
            if (action is StayOffAction)
                action.Deactivate();
        }
    }
}
