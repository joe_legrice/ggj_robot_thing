﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Switch off something if something is pressed and hold
public class StayOffAction : ButtonAction
{
    [SerializeField]
    Trap linkedTrap;
    
    public override void Activate()
    {
        linkedTrap.Switch(true);
    }

    public override void Deactivate()
    {
        linkedTrap.Switch(false);
    }
}
