﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Level Data")]
public class LevelData : ScriptableObject
{
    [System.Serializable]
    public class LevelInfo
    {
        public string m_levelName;
        public bool m_isCogScene;
        public string m_levelSceneName;
    }

    [SerializeField] private LevelInfo[] m_allLevels;

    public int NumberOfLevels { get { return m_allLevels.Length; } }

    public LevelInfo GetLevelInfo(int index)
    {
        return m_allLevels[index];
    }

    public int GetLevelIndex(LevelInfo li)
    {
        return System.Array.IndexOf(m_allLevels, li);
    }
}
