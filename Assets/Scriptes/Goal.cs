﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goal : MonoSingleton<Goal>
{
    [SerializeField] private int neededRobots = 2;

    private int m_numberOfRobotsSaved = 0;

    public int NumberOfRobotsSaved { get { return m_numberOfRobotsSaved; } }
    public int NumberOfRobotsNeeded { get { return neededRobots; } }
        

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Robot"))
        {
            Robot r = other.GetComponent<Robot>();
            if (r.SetInGoal())
            {
                m_numberOfRobotsSaved++;
            }
        }
    }
}
