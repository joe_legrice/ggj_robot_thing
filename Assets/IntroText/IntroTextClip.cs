using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class IntroTextClip : PlayableAsset, ITimelineClipAsset
{
    public IntroTextBehaviour template = new IntroTextBehaviour ();

    public ClipCaps clipCaps
    {
        get { return ClipCaps.Blending; }
    }

    public override Playable CreatePlayable (PlayableGraph graph, GameObject owner)
    {
        var playable = ScriptPlayable<IntroTextBehaviour>.Create (graph, template);
        return playable;
    }
}
