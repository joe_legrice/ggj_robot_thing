using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using UnityEngine.UI;

[Serializable]
public class IntroTextBehaviour : PlayableBehaviour
{
    public string text = "A fire broke down in the Server Room!";
}
