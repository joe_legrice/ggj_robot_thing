using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class fireFADEClip : PlayableAsset, ITimelineClipAsset
{
    public fireFADEBehaviour template = new fireFADEBehaviour ();

    public ClipCaps clipCaps
    {
        get { return ClipCaps.Blending; }
    }

    public override Playable CreatePlayable (PlayableGraph graph, GameObject owner)
    {
        var playable = ScriptPlayable<fireFADEBehaviour>.Create (graph, template);
        return playable;
    }
}
