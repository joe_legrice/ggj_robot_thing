using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class fireFADEBehaviour : PlayableBehaviour
{
    public bool enableEmission = true;
}
