using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class fireFADEMixerBehaviour : PlayableBehaviour
{
    bool m_DefaultEnableEmission;

    ParticleSystem m_TrackBinding;
    bool m_FirstFrameHappened;

    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {
        m_TrackBinding = playerData as ParticleSystem;

        if (m_TrackBinding == null)
            return;

        if (!m_FirstFrameHappened)
        {
            m_DefaultEnableEmission = m_TrackBinding.enableEmission;
            m_FirstFrameHappened = true;
        }

        int inputCount = playable.GetInputCount ();

        float totalWeight = 0f;
        float greatestWeight = 0f;
        int currentInputs = 0;

        for (int i = 0; i < inputCount; i++)
        {
            float inputWeight = playable.GetInputWeight(i);
            ScriptPlayable<fireFADEBehaviour> inputPlayable = (ScriptPlayable<fireFADEBehaviour>)playable.GetInput(i);
            fireFADEBehaviour input = inputPlayable.GetBehaviour ();
            
            totalWeight += inputWeight;

            if (inputWeight > greatestWeight)
            {
                m_TrackBinding.enableEmission = input.enableEmission;
                greatestWeight = inputWeight;
            }

            if (!Mathf.Approximately (inputWeight, 0f))
                currentInputs++;
        }

        if (currentInputs != 1 && 1f - totalWeight > greatestWeight)
        {
            m_TrackBinding.enableEmission = m_DefaultEnableEmission;
        }
    }

    public override void OnPlayableDestroy (Playable playable)
    {
        m_FirstFrameHappened = false;

        if (m_TrackBinding == null)
            return;

        m_TrackBinding.enableEmission = m_DefaultEnableEmission;
    }
}
